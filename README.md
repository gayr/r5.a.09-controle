# r5.a.09-controle

## Prérequis

Installer Loki à la racine du repository : 

```bash

docker plugin install grafana/loki-docker-driver:latest --alias logi --grant-all-permissions
```
## Treafik 

Pour commencer, j'ai créé un fichier docker-compose.yml qui contient les services suivants :
- Traefik  

### Pour le lancer, il suffit d'aller dans le répertoire treafik et lancer dans le terminal un docker-compose up -d

Pour ensuite accéder à Traefik, il faut aller sur l'adresse suivante : http://localhost:8080

## Loki 

Pour commencer, j'ai créé un fichier docker-compose.yml qui contient les services suivants :
- Loki 

### Pour le lancer, il suffit d'aller dans le répertoire loki et lancer dans le terminal un docker-compose up -d

Pour ensuite accéder à Grafana , il faut aller sur l'adresse suivante : http://localhost:3000/?orgId=1

## Ghost

Pour commencer, j'ai créé un fichier docker-compose.yml qui contient les services suivants :
- Ghost
- MariaDB

### Pour le lancer, il suffit d'aller dans le répertoire ghost et lancer dans le terminal un docker-compose up -d

Pour ensuite accéder à Grafana , il faut aller sur l'adresse suivante : http://localhost:2368/
## Joomla

Pour commencer, j'ai créé un fichier docker-compose.yml qui contient les services suivants :
- Joomla

### Pour le lancer, il suffit d'aller dans le répertoire joomla et lancer dans le terminal un docker-compose up -d

## Gitea

Pour commencer, j'ai créé un fichier docker-compose.yml qui contient les services suivants :
- Gitea
- MariaDB

### Pour le lancer, il suffit d'aller dans le répertoire gitea et lancer dans le terminal un docker-compose up -d

## matomo

Pour commencer, j'ai créé un fichier docker-compose.yml qui contient les services suivants :
- matomo
- MariaDB

### Pour le lancer, il suffit d'aller dans le répertoire matomo et lancer dans le terminal un docker-compose up -d
